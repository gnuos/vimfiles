source ~/.vim/configs/basic.vim
source ~/.vim/configs/filetypes.vim
source ~/.vim/configs/plugins.vim
source ~/.vim/configs/mapkeys.vim
source ~/.vim/configs/extended.vim

