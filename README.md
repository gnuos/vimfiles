# vimfiles

#### 介绍
结构化的 vim 配置文件，主要用于 Python、Ruby、Golang、Crystal 的代码编辑，本 vim 配置使用了 pathogen 管理插件


#### 安装教程

Linux 或 MacOS 环境执行下列命令安装：

```bash
git clone --recurse-submodules https://git.10gb.io/kevin/vimfiles.git ~/.vim 

# 境内用户可以执行下面的命令从码云上面拉取
# git clone --recurse-submodules https://gitee.com/gnuos/vimfiles.git ~/.vim

ln -fsv ~/.vim/vimrc ~/.vimrc
```

Windows 环境在 Git-Bash 提示符中执行下列命令安装：

```bash
git clone --recurse-submodules https://git.10gb.io/kevin/vimfiles.git ~/vimfiles

# 境内用户可以执行下面的命令从码云上面拉取
# git clone --recurse-submodules https://gitee.com/gnuos/vimfiles.git ~/vimfiles

```


#### 使用说明

以下是本 Vim 配置的结构说明

```
├── autoload
│   └── pathogen.vim    # pathogen 插件管理器的自动加载路径
├── configs
│   ├── basic.vim       # 可脱离插件的 Vim 基本配置
│   ├── extended.vim    # 对 Vim 的默认配置进行的一些魔改
│   ├── filetypes.vim   # 对文件类型进行检测，并设置缩进和高亮
│   ├── mapkeys.vim     # 设置插件相关的按键绑定，以及添加一些很有用的快捷键
│   └── plugins.vim     # 各个插件的配置参数
├── del_plugin.sh       # 用于移除指定插件的脚本，参数是插件的 bundle 路径
├── update.sh           # 用于手动更新所有插件的脚本
└── vimrc               # 整个Vim配置的入口，用于引入 configs 目录中的配置

```

如果你要增加一些插件，请按照本项目的 README 指导文档发起 Pull Request，添加插件的方式示例如下：

1.  通过 Fork 本项目创建你自己的Vim配置（推荐）

2.  通过提交 Issue 讨论添加你需要的插件

3.  通过提交代码的方式（需要熟悉 Vim 的一些配置参数）

1)  首先你需要申请加入到本项目成为项目的开发者成员
2)  你需要执行下列命令提交你要添加的插件

```bash
cd ~/.vim

git checkout -b need_clojure master

git submodule add https://github.com/guns/vim-clojure-static.git bundle/vim-clojure

#......

# 在 configs 目录中的文件里添加你需要优化的配置参数

#......

git commit -m '增加Clojure语言的支持插件'

git push orgin need_clojure:need_clojure

```

3)  在仓库页面中创建一个 Pull Request，并且描述这个插件的特性
4)  等待合并到 master 分支给大家使用


#### 参与贡献

1.  Fork 本仓库
2.  新建 need_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
