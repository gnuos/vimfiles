execute pathogen#infect()
execute pathogen#helptags()

syntax enable
filetype plugin indent on            

set ruler
set magic
set mouse-=a
set showcmd
set showmatch
set smartcase
set incsearch
set hlsearch
set autoread
set wildmenu
set wildignore=*.o,*~,*.pyc
set backspace=eol,start,indent
set laststatus=2
set cmdheight=1
set lazyredraw
set encoding=utf8
set ffs=unix,dos,mac
set pastetoggle=<F11>
set background=dark

set noerrorbells
set novisualbell
set tm=500
set t_vb=
set t_Co=256

set nobackup
set nowb
set noswapfile

set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" 快捷键映射
let mapleader=','
nmap <leader>w :w!<cr>

map <silent> <leader><cr> :noh<cr>

" :W sudo saves the file
command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

" 记下最后的编辑位置
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove 
map <leader>t<leader> :tabnext

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif

